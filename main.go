package main

import (
	"strings"

	"github.com/thatisuday/commando"
	"gitlab.com/bitify-pub/gomssql/gomssql"
)

// main function that accepts string argument from command line
func main() {

	// read command line arguments
	commando.
		SetExecutableName("gomssql").
		SetVersion("1.0.0").
		SetDescription("A simple command line tool to execute SQL statements on MariaDB and MSSQL databases")

	commando.
		Register(nil).
		AddFlag("dburi,U", "database URI Eg: `root:mariadb@tcp(127.0.0.1)` (must be in quote)", commando.String, nil).
		AddFlag("driver,D", "database driver [mssql|mariadb]", commando.String, "mariadb").
		AddFlag("database,d", "database name", commando.String, nil).
		AddFlag("sql,s", "SQL statement", commando.String, "-").
		AddFlag("file,f", "SQL file", commando.String, "-").
		SetAction(func(args map[string]commando.ArgValue, flags map[string]commando.FlagValue) {
			driver := flags["driver"].Value.(string)
			dbname := flags["database"].Value.(string)

			// one of the two flags must be provided, if both are provided, sql flag will be used
			sql := flags["sql"].Value.(string)
			file := flags["file"].Value.(string)

			if file == "-" && sql == "-" {
				println("No SQL or file provided")
				return
			}

			var dbdriver gomssql.AbstractDriver
			if driver == "mssql" {
				dbdriver.Drive = &gomssql.MssqlDriver{
					DbUri: flags["dburi"].Value.(string),
				}
			} else if driver == "mariadb" || driver == "mysql" {
				dbdriver.Drive = &gomssql.MariadbDriver{
					DbUri: flags["dburi"].Value.(string),
				}
			} else {
				println("Invalid driver provided")
				return
			}

			if sql != "-" {
				runSql(dbdriver, dbname, sql)
			} else if file != "-" && strings.HasSuffix(file, ".sql") {
				runSqlFile(dbdriver, file)
			} else {
				println("Missing SQL, or invalid file provided")
			}
		})

	commando.Parse(nil)
}

func runSql(driver gomssql.AbstractDriver, dbname string, sql string) {
	if dbname == "" {
		println("No table name provided")
		return
	}
	if sql == "" {
		println("No SQL provided")
		return
	}

	driver.ExecuteSql(dbname, sql)
}

func runSqlFile(driver gomssql.AbstractDriver, file string) {
	if file == "" {
		println("No file provided")
		return
	}

	_, found := strings.CutSuffix(file, ".sql")
	if !found {
		println("Invalid file provided")
		return
	}

	driver.ExecuteFile(file)
}
