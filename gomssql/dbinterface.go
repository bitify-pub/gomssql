package gomssql

import (
	"bufio"
	"database/sql"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Driver interface {
	GetConnStr() string
	GetDriverName() string
}

type AbstractDriver struct {
	Drive Driver
}

const (
	singleCommentToken = "--"
	beginCommentToken  = "/*"
	endCommentToken    = "*/"
)

func (driver *AbstractDriver) ExecuteFile(path string) {
	// read file line by line. Each line is a SQL statement terminated by ';'
	file, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	stmt := "" // buffer to store clean SQL statement terminated by ';'
	for scanner.Scan() {
		str := scanner.Text()

		// remove leading and trailing spaces, and handle single line comments
		str = strings.TrimSpace(str)
		_, iscomment := strings.CutPrefix(str, singleCommentToken)
		if str == "" || iscomment {
			continue
		}

		// handle comment after SQL statement
		if strings.Contains(str, singleCommentToken) {
			str = strings.Split(str, singleCommentToken)[0]
		}

		// if sql contains ';' we assume it is in the middle of a statement
		if strings.Contains(str, ";") {
			frac := strings.Split(str, ";")
			stmt += frac[0]
			driver.ExecuteSql("", stmt)
			stmt = frac[1]
		} else {
			stmt += " " + str
		}
	}

	// handle multiline comments by joining all lines into 1 and removing string between '/*' and '*/'
	// while true
	for {
		if strings.Contains(stmt, beginCommentToken) {
			// if there is a begin comment token, we need to find the end comment token
			if strings.Contains(stmt, endCommentToken) {
				// if there is an end comment token, we remove the string between the two tokens
				beginIdx := strings.Index(stmt, beginCommentToken)
				endIdx := strings.Index(stmt, endCommentToken)

				frac := make([]string, 0)
				frac = append(frac, stmt[:beginIdx])
				frac = append(frac, stmt[endIdx+len(endCommentToken):])

				stmt = strings.Join(frac, " ")
			}
		} else {
			break
		}

	}

	if strings.TrimSpace(stmt) != "" {
		stmt = strings.Replace(stmt, "  ", " ", -1)

		if len(stmt) == 0 {
			panic("No SQL statement found")
		}

		driver.ExecuteSql("", stmt)
	}
}

func (driver *AbstractDriver) ExecuteSql(dbname, str string) {
	// db, err := sql.Open("mssql", "server=116.204.203.191;user id=sa;password=;")
	db, err := sql.Open(driver.Drive.GetDriverName(), driver.Drive.GetConnStr())
	if err != nil {
		panic(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	rows, err := db.Query(str)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	types, err := rows.ColumnTypes()
	if err != nil {
		panic(err)
	}

	printColumnNames(types)

	for rows.Next() {
		vals := make([]interface{}, len(types)) // value array to store data
		for i := range types {
			vals[i] = new(interface{})
		}

		err = rows.Scan(vals...)
		if err != nil {
			panic(err)
		}

		for n, val := range vals {
			strval := ""
			x := *val.(*interface{})

			switch types[n].DatabaseTypeName() {
			case "INT":
				if x == nil {
					fmt.Printf("| %-10s", "NULL")
					continue
				}
				strval += strconv.Itoa(int(x.(int64)))
				fmt.Printf("| %-10s", strval)
			case "BOOL":
				if x == nil {
					fmt.Printf("| %-5s", "NULL")
					continue
				}
				strval += fmt.Sprintf("%t", *val.(*interface{}))
				fmt.Printf("| %-5s", strval)
			case "DECIMAL":
				if x == nil {
					fmt.Printf("| %-10s", "NULL")
					continue
				}
				strval += fmt.Sprintf("%f", *val.(*interface{}))
				fmt.Printf("| %-10s", strval)
			default:
				if x == nil {
					fmt.Printf("| %-20s", "NULL")
					continue
				}
				strval += fmt.Sprintf("%s", *val.(*interface{}))
				fmt.Printf("| %-20s", strval)
			}
		}
		fmt.Println()
	}
}

func printColumnNames(types []*sql.ColumnType) {
	// combine column names into a string
	colnames := ""
	for n, col := range types {
		switch types[n].DatabaseTypeName() {
		case "INT":
			colnames += fmt.Sprintf("| %-10s", col.Name() + "(INT)")
		case "BOOL":
			colnames += fmt.Sprintf("| %-5s", col.Name() + "(BOOL)")
		case "DECIMAL":
			colnames += fmt.Sprintf("| %-10s", col.Name() + "(DEC)")
		default:
			colnames += fmt.Sprintf("| %-20s", col.Name() + "(STR)")
		}
		// colnames += fmt.Sprintf("| %-20s", col.Name())
	}
	colnames += "|"

	strLine := strings.Repeat("-", len(colnames))

	fmt.Println(strLine)
	fmt.Println(colnames)
	fmt.Println(strLine)
}
