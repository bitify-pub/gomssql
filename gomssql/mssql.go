package gomssql

import (
	_ "github.com/denisenkom/go-mssqldb"
)

type MssqlDriver struct {
	DbUri string
}

func (m * MssqlDriver) GetConnStr() string {
	return m.DbUri
	// return "server=116.204.203.191;user id=sa;"
}

func (m *MssqlDriver) GetDriverName() string {
	return "mssql"
}
