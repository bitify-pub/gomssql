package gomssql

import (
	_ "github.com/go-sql-driver/mysql"
)

type MariadbDriver struct {
	DbUri string
}

func (m * MariadbDriver) GetConnStr() string {
	return m.DbUri
}

func (m *MariadbDriver) GetDriverName() string {
	return "mysql"
}

